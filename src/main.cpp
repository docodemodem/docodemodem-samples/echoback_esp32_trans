/*
 * Sample program for DocodeModem
 * transmit and wait echoback
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>

#define TRANSMIT_COUNT 10

const uint8_t CHANNEL = 0x10;   //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x11; //通信相手のIDです。
const uint8_t DEVICE_EI = 0x10; // 自分のIDです。
const uint8_t DEVICE_GI = 0x02; //グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

int trans_count = 0;
int rcv_count = 0;
short env_rssi[TRANSMIT_COUNT] = {0};        //送信側の環境RSSI値
short rcv_rssi[TRANSMIT_COUNT] = {0};        //受信時のRSSI値
short target_env_rssi[TRANSMIT_COUNT] = {0}; //受信側の環境RSSI値
short target_rcv_rssi[TRANSMIT_COUNT] = {0}; //受信側の受信時のRSSI値

short ave_calc(short *data,int count)
{
  short ave = 0;
  int ave_count = 0;

  for (int i = 0; i < count; i++)
  {

    if (data[i] != 0)
    {
      ave += data[i];
      ave_count++;
    }
  }
  if (ave_count!=0){

    return ave / ave_count;
  }else{
    return 0;
  }

}

void do_test()
{
  char data[10];

  for (int count = 0; count < TRANSMIT_COUNT; count++)
  {
    SerialDebug.printf("Transmit %d\r\n",count);

    //送信データ作成。下記の場合４バイトのASCIIですが、Binaryでも構いません。
    int size = sprintf(data, "%4d", count);

    //環境RSSI値を取得します
    modem.GetRssiCurrentChannel(&env_rssi[count]);

    //@DT04****\r\nを実行し、送信結果を戻します。@DT04と\r\nは自動で付加されます。
    auto rc = modem.TransmitData((uint8_t *)data, size);
    if (rc == SlrModemError::Ok)
    {
      //送信完了
      SerialDebug.printf("Send Ok %d\r\n",count);
      trans_count++;

      //エコーバックを待ちます
      int timeout = millis();
      while (1)
      {
        modem.Work();

        if (modem.HasPacket())
        {
          //受信時のRSSI値取得
          modem.GetRssiLastRx(&rcv_rssi[count]);

          const uint8_t *pData;
          uint8_t len{0};
          modem.GetPacket(&pData, &len);

          if (len == 8) //size check
          {
            uint16_t recvdata[4];
            memcpy(&recvdata[0], pData, len);
            target_env_rssi[count] = recvdata[2];
            target_rcv_rssi[count] = recvdata[3];
            rcv_count++;
            SerialDebug.printf("Receive Ok %d\r\n", count);
          }

          modem.DeletePacket();
          break;
        }

        if (millis() - timeout > 5000)
        {
          //５秒以内に戻ってこなかったので届かなったようです
          SerialDebug.println("receive timeout...");
          break;
        }
      }
    }
    else
    {
      //キャリアセンスによって送信できなかったことを示します
      SerialDebug.println("Send Ng...");
    }
  }

  //結果表示
  SerialDebug.println("\r\nresult:");
  SerialDebug.printf("Transmit OK %d / %d\r\n", trans_count, TRANSMIT_COUNT);
  SerialDebug.printf("Received %d / %d\r\n", rcv_count, trans_count);

  short env_rssi_ave = ave_calc(env_rssi, TRANSMIT_COUNT); //送信側の環境RSSI値平均
  short rcv_rssi_ave = ave_calc(rcv_rssi, TRANSMIT_COUNT); //送信側の受信時のRSSI値平均
  short target_env_rssi_ave = ave_calc(target_env_rssi,TRANSMIT_COUNT); //受信側の環境RSSI値平均
  short target_rcv_rssi_ave = ave_calc(target_rcv_rssi, TRANSMIT_COUNT); //受信側の受信時のRSSI値平均

  SerialDebug.printf("RSSI Transmit side Average Env %ddBm, Rcv %ddBm\r\n", env_rssi_ave, rcv_rssi_ave);
  SerialDebug.printf("RSSI EchoBack side Average Env %ddBm, Rcv %ddBm\r\n", target_env_rssi_ave, target_rcv_rssi_ave);
}

void setup()
{
  Dm.begin(); //初期化が必要です。

  //デバッグ用シリアルの初期化です
  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  //モデム用シリアルの初期化です。通信速度とポート番号を注意してください。
  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  do_test();
}



void loop()
{
}